<?php

namespace App\Models;
class CronModel extends BaseModel
{
    protected $table = 'cron_job';
    protected $primaryKey = 'id';

    protected $useSoftDeletes = false;
    protected $allowedFields = ['process_name','is_processing'];
    /**
     * @param string|null $scenario
     * @return array
     */
    public function getRules(string $scenario = null): array
    {
        return [

        ];
    }
}