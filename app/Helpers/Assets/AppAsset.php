<?php

namespace App\Helpers\Assets;


class AppAsset extends BaseAsset
{
    public $css = [
        'css/main.css?v=0.2.0'
    ];

    public $js = [
        ['https://unpkg.com/react@16/umd/react.development.js', 'crossorigin'],
        ['https://unpkg.com/react-dom@16/umd/react-dom.development.js', 'crossorigin'],
        'admin/js/shopping_cart.js?v=0.0.1',
        'js/main.js?v=0.0.8',
        ['https://www.google.com/recaptcha/api.js','async']
    ];

    public $depends = [
        Bootstrap3Asset::class,
        LightSliderAsset::class,
        SwiperAsset::class,
        JqueryToastAsset::class,
        FontAwesomeAsset::class,
        VotesAsset::class
    ];
}