<?php
use App\Helpers\Html;
/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\ProjectModel $model
 * @var \CodeIgniter\Validation\Validation $validator
 */


$this->title = 'Cấu hình âm thanh';
?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-info flex-align">
                <div>
                    <h4 class="card-title"><?= $this->title ?></h4>
                </div>
            </div>
            <div class="card-body">
                <form action="<?= route_to('ld_voice_option') ?>" method="post"
                      enctype="multipart/form-data">

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="bmd-label-floating">Số lần thông báo</label>
                                <?= Html::input('number','number_alert', $model->number_alert, [
                                    'autocomplete' => 'off',
                                    'class' => 'form-control',
                                    'autofocus' => true,
                                ]) ?>
                            </div>


                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="bmd-label-floating">Khoảng thời gian giữa các thông báo (giây)</label>
                                <?= Html::input('number','alert_time_loop', $model->alert_time_loop, [
                                    'autocomplete' => 'off',
                                    'class' => 'form-control'
                                ]) ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="bmd-label-floating">Thời gian chờ vào (phút)</label>
                                <?= Html::input('number','time_wait_come_in', $model->time_wait_come_in, [
                                    'autocomplete' => 'off',
                                    'class' => 'form-control'
                                ]) ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group" style="margin-top: -8px">
                                <label class="bmd-label-floating">Kểu thông báo</label>
                                <select class="form-control" name="type_alert">
                                    <option value="bien_so" <?php if($model->type_alert == 'bien_so') echo 'selected'?> >Biển số</option>
                                    <option value="so_thu_tu" <?php if($model->type_alert == 'so_thu_tu') echo 'selected'?> >Số thứ tự</option>
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="card-bottom-actions">
                        <div class="flex-row" id="add-holder"></div>
                        <a href="<?= route_to('ld_voice_option') ?>"  class="btn btn-round">Huỷ</a>
                        <button class="btn btn-info btn-round" type="submit">Lưu</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>