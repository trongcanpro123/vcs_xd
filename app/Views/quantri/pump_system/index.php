<?php

use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\ProjectModel[] $models
 * @var \CodeIgniter\Pager\Pager $pager
 */

$this->title = 'Quản lý giàn bơm';
?>
<div class="card">
    <div class="card-header card-header-info flex-align">
        <div>
            <h4 class="card-title"><?= $this->title ?></h4>
        </div>
        <a href="<?= route_to('nv_pump_system_create') ?>" class="btn btn-warning btn-round btn-sm">Thêm mới</a>
    </div>
    <div class="card-body table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Kho</th>
                <th>Tên giàn</th>
                <th>Hành động</th>
            </tr>
            </thead>
            <tbody>
            <?php if (!$models || empty($models)): ?>
                <tr>
                    <td colspan="100">
                        <div class="empty-block">
                            <img src="/images/no-content.jpg" alt="No content"/>
                            <h4>Không có nội dung</h4>
                            <a class="btn btn-info btn-round"
                               href="<?= route_to('nv_pump-system_create') ?>">Thêm</a>
                        </div>
                    </td>
                </tr>
            <?php else: ?>
                <?php foreach ($models as $model): ?>
                    <tr>
                        <td><?= Html::decode($model->select_name_area()) ?></td>
                        <td><?= $model->pump_name ?></td>
                        <td class="row-actions">
                            <?= Html::a('<i class="material-icons">edit</i>', ['nv_pump_system_update', $model->getPrimaryKey()], [
                                'class' => ['btn', 'btn-info', 'btn-just-icon','btn-sm'],
                                'title' => 'Sửa'
                            ]) ?>
                            <a href="<?= route_to('nv_pump_system_delete', $model->getPrimaryKey()) ?>"
                               class="btn btn-danger btn-just-icon btn-sm" data-method="post"
                               data-prompt="Bạn có chắc sẽ xoá đi mục này?">
                                <i class="material-icons">delete</i>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif ?>
            </tbody>
        </table>
        <?= $pager->links('default', 'default_cms') ?>
    </div>
</div>
