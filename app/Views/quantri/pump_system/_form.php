<?php

use App\Helpers\Html;

/**
 * @var \App\Models\AdministratorModel $model
 */
?>
<div class="form-group">
    <label class="bmd-label-floating">Kho</label>
    <select class="form-control" name="area_id" re>
                <option value="<?=$model_area->id?>"
                    <?php if ($model_area->id == $model_name){
                        echo 'selected';
                    }?>
                ><?=$model_area->name?></option>
    </select>
</div>
<div class="form-group">
    <label class="bmd-label-floating">Tên giàn bơm</label>
    <input type="text" name="pump_name" value="<?=$model->pump_name?>" autocomplete="off" class="form-control">
</div>
